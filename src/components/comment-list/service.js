export default class ComService {
  constructor($http) {
    this.$http = $http;
  }

  list() {
    return this.$http.get(`${API}/comments`)
      .then(response => {
        return response.data;
      })
      .catch(e => {
        alert(JSON.stringify(e));
      });
  }

  remove(comment) {
    return this.$http.delete(`${API}/comments/${comment.id}`)
      .then(response => {
        return response.data;
      })
      .catch(e => {
        alert(JSON.stringify(e));
      });
  }

  add() {
    let comment = {
      'id': '',
      'text': '',
      'rate': '0',
      'author': 'Username'
    };
    return this.$http.post(`${API}/comments/`, comment)
      .then(response => {
        return response.data;
      })
      .catch(e => {
        alert(JSON.stringify(e));
      });
  }

  plus(comment) {
    comment.rate++;
    return this.$http.put(`${API}/comments/${comment.id}`, comment)
      .then(response => {
        return response.data;
      })
      .catch(e => {
        alert(JSON.stringify(e));
      });
  }

  minus(comment) {
    comment.rate--;
    return this.$http.put(`${API}/comments/${comment.id}`, comment)
      .then(response => {
        return response.data;
      })
      .catch(e => {
        alert(JSON.stringify(e));
      });
  }

  edit(comment) {
    return this.$http.put(`${API}/comments/${comment.id}`, comment)
      .then(response => {
        return response.data;
      })
      .catch(e => {
        alert(JSON.stringify(e));
      });
  }
}
