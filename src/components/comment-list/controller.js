export default class ComListController {
  constructor(ComService) {
    'ngInject';
    this.ComService = ComService;
  }

  $onInit() {
    this.ComService.list().then(result => {
      this.items = result;
      return this.items;
    });
  }

  remove(comment) {
    this.ComService.remove(comment).then(result => {
      this.items = result;
      this.$onInit();
    });
  }

  add() {
    this.ComService.add().then(result => {
      this.item = result;
      this.$onInit();
    });
  }
}
