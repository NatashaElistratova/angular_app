export default class ComController {
  constructor(ComService) {
    'ngInject';
    this.ComService = ComService;
  }

  remove() {
    this.ComListController.remove(this.item);
  }

  $onInit() {
    this.ComListController.$onInit();
  }

  plus() {
    this.ComService.plus(this.item).then(result => {
      this.item = result;
      this.$onInit();
    });
  }

  minus() {
    this.ComService.minus(this.item).then(result => {
      this.item = result;
      this.$onInit();
    });
  }

  edit() {
    this.ComService.edit(this.item).then(result => {
      this.item = result;
      this.$onInit();
    });
  }
}
