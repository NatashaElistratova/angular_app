import ng from 'angular';
import CommentDirective from './component';
import ComService from '../service';

export default ng.module('app.comment', [])
    .service('ComService', ComService)
    .component('comment', CommentDirective)
    .name;
