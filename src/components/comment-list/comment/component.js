import template from './template.html';
import controller from './controller';

export default {
  restrict: 'E',
  controller,
  controllerAs: '$ComCtrl',
  template,
  bindings: {
    item: '='
  },
  require: {
    ComListController: '^^commentList'
  }
};
