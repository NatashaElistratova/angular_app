
  import ng from 'angular';
  import 'bootstrap/dist/css/bootstrap.css';
  import 'font-awesome/css/font-awesome.css';
  import ComService from './service';
  import commentListDirective from './component';


  export default ng.module('app.comment-list', [])
           .service('ComService', ComService)
           .component('commentList', commentListDirective)
           .name;
