import controller from './controller';
import template from './template.html';

export default {
  restrict: 'E',
  controller,
  controllerAs: '$ctrlSearch',
  template
};
