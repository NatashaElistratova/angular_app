import ng from 'angular';
import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.css';
import SearchDirective from './component';

export default ng.module('app.search', [])
    .component('search', SearchDirective)
    .name;
