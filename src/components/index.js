import ng from 'angular';
import commentList from './comment-list';
import comment from './comment-list/comment';
import search from './search';
import sidebar from './sidebar';
import photoList from './photo-list';
import photo from './photo-list/photo';

export default ng.module('app.components', [commentList, comment, search, sidebar, photoList, photo]);
