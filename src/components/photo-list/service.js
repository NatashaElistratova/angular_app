export default class PhotoService {
  constructor($http) {
    this.$http = $http;
  }

  photoList() {
    return this.$http.get(`${API}/photos`)
      .then(response => {
        return response.data;
      })
      .catch(e => {
        alert(JSON.stringify(e));
      });
  }

  plus(photo) {
    photo.rate++;
    return this.$http.put(`${API}/photos/${photo.id}`, photo)
      .then(response => {
        return response.data;
      })
      .catch(e => {
        alert(JSON.stringify(e));
      });
  }

  minus(photo) {
    photo.rate--;
    return this.$http.put(`${API}/photos/${photo.id}`, photo)
      .then(response => {
        return response.data;
      })
      .catch(e => {
        alert(JSON.stringify(e));
      });
  }

}
