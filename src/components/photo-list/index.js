import ng from 'angular';
import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.css';
import PhotoService from './service';
import photoListDirective from './component';


export default ng.module('app.photo-list', [])
         .service('PhotoService', PhotoService)
         .component('photoList', photoListDirective)
         .name;
