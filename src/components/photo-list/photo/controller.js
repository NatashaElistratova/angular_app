export default class PhotoController {
  constructor(PhotoService) {
    'ngInject';
    this.PhotoService = PhotoService;
  }

  $onInit() {
    this.PhotoListController.$onInit();
  }

  plus() {
    this.PhotoService.plus(this.item).then(result => {
      this.item = result;
      this.$onInit();
    });
  }

  minus() {
    this.PhotoService.minus(this.item).then(result => {
      this.item = result;
      this.$onInit();
    });
  }

}
