import ng from 'angular';
import PhotoDirective from './component';
import PhotoService from '../service';

export default ng.module('app.photo', [])
    .service('PhotoService', PhotoService)
    .component('photo', PhotoDirective)
    .name;
