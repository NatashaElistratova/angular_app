export default class PhotoListController {
  constructor(PhotoService) {
    'ngInject';
    this.PhotoService = PhotoService;
  }

  $onInit() {
    this.PhotoService.photoList().then(result => {
      this.items = result;
      return this.items;
    });
  }

}
