    import ng from 'angular';
    import SidebarDirective from './component';

    export default ng.module('app.sidebar', [])
        .component('sidebar', SidebarDirective)
        .name;
