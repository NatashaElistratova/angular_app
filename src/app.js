import ng from 'angular';
import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.css';
import 'angular-ui-router';
import './styles.css';
import components from './components';

ng.module('app', [components, 'ui.router'])
  .config(($stateProvider, $urlRouterProvider) => {
    $stateProvider
    .state('home', {
      url: '/home',
      templateUrl: 'src/components/home/template.html'
    })
    .state('manual', {
      url: '/uchebnik',
      templateUrl: 'src/components/manual/template.html'
    })
    .state('comments', {
      url: '/kommentarii',
      template: '<comment-list></comment-list>'
    })
    .state('photo', {
      url: '/photo',
      template: '<photo-list></photo-list>'
    });
    $urlRouterProvider.otherwise('/');
  });
